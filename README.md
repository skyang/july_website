Setup:

virtualenv july

cd july

source bin/activate

pip install flask_mongoengine  # this will install all dependent python packages

mkdir data                     # dbpath for mongodb

git clone https://skyang@bitbucket.org/skyang/july_website.git

